from __future__ import print_function
import argparse

import torch
import torch.optim as optim

from utils.nn import AdamNormGrad

import os

import datetime
from utils.load_data import load_dataset

# # # # # # # # # # #
# START EXPERIMENTS # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # #
#

# Training settings
parser = argparse.ArgumentParser(description='CP-VAE')

# arguments for optimization
parser.add_argument('--batch_size', type=int, default=100, metavar='BStrain',
                    help='input batch size for training ')
parser.add_argument('--test_batch_size', type=int, default=100, metavar='BStest',
                    help='input batch size for testing ')
parser.add_argument('--epochs', type=int, default=0, metavar='E',
                    help='number of epochs to train ')

parser.add_argument('--lr', type=float, default=0.0005, metavar='LR',
                    help='learning rate (default: 0.0005)')
parser.add_argument('--early_stopping_epochs', type=int, default=50, metavar='ES',
                    help='number of epochs for early stopping')

parser.add_argument('--warmup', type=int, default=100, metavar='WU',
                    help='number of epochs for warmu-up')
# warmup is used to boost the generative capacity of the decoder

parser.add_argument('--beta', type=float, default= 1.0,
                    help='hyperparameter to scale KL term in ELBO')

# Gumbel-Softmax parametes

parser.add_argument('--temp', type=float, default=1.0, metavar='TEMP',
                    help='Gumbel-Softmax initial temperature (default: 1.0)')

parser.add_argument('--temp_min', type=float, default=0.5, metavar='TEMP_MIN',
                    help='minimum Gumbel-Softmax temperature (default: 0.5)')

parser.add_argument('--anneal_rate', type=float, default=0.00003, metavar='ANR',
                    help='annealing rate for Gumbel-Softmax (default: 0.00003)')
parser.add_argument('--anneal_interval', type=float, default=1, metavar='ANIN',
                    help='annealing interval for Gumbel-Softmax  (default: 100)')


# cuda
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='enables CUDA training')
# random seed
parser.add_argument('--seed', type=int, default=14, metavar='S',
                    help='random seed ')
# model: latent size, input_size, so on


parser.add_argument('--z1_size', type=int, default= 1, metavar='M1',
                    help='latent size')
parser.add_argument('--disc_size', type=int, default= 1 , metavar='M2',
                    help='discrete latent size/dim')

parser.add_argument('--input_size', type=int, default=[1, 28, 28], metavar='D',
                    help='input size')
parser.add_argument('--hidden_size', type=int, default= 300, metavar='D',
                    help='hidden size')

parser.add_argument('--activation', type=str, default=None, metavar='ACT',
                    help='activation function')


# model: model name, prior
parser.add_argument('--model_name', type=str, default='vae', metavar='MN',
                    help='model name: vae, vae_z_xc, vae_z_x')

parser.add_argument('--prior', type=str, default='standard', metavar='P',
                    help='prior: standard, conditional, MoG')

parser.add_argument('--input_type', type=str, default='binary', metavar='IT',
                    help='type of the input: binary, gray, continuous')
# dataset
parser.add_argument('--dataset_name', type=str, default='fashion_mnist', metavar='DN',
                    help='name of the dataset: dynamic_mnist, fashion_mnist,'
                         'omniglot')

parser.add_argument('--dynamic_binarization', action='store_true', default=False,
                    help='allow dynamic binarization')

# reconstruction
parser.add_argument('--no_recon_oneHot', action='store_true', default=False,
                    help='enables to pick the most likely sample to reconstruct the input (in validation - test)')


args = parser.parse_args()
args.recon_oneHot = not args.no_recon_oneHot
args.cuda = not args.no_cuda and torch.cuda.is_available()

torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)

kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def run(args, kwargs):
    args.model_signature = str(datetime.datetime.now())[0:19]

    model_name = args.dataset_name + '_' + args.model_name + '_' + args.prior + '(K_' + str(args.number_components)\
                     + ')' + '_wu(' + str(args.warmup) + ')' + '_z1_' + str(args.z1_size) + '_c_' \
                     + str(args.disc_size) +'_beta_'+ str(args.beta) + '_lr_' + str(args.lr)


    # DIRECTORY FOR SAVING
    snapshots_path = 'snapshots/'
    dir = snapshots_path
    
    if not os.path.exists(dir):
        os.makedirs(dir)

    # LOAD DATA=========================================================================================================
    print('load data')

    # loading data
    train_loader, val_loader, test_loader, args = load_dataset(args, **kwargs)

    # CREATE MODEL======================================================================================================
    print('create model')
    # importing model
    if args.model_name == 'vae':
        from models.vae import VAE
    elif args.model_name == 'vae_z_x':
        from models.vae_z_x import VAE
    elif args.model_name == 'vae_z_xc':
        from models.vae_z_xc import VAE
    else:
        raise Exception('Wrong name of the model!')

    model = VAE(args)
    if args.cuda:
        model.cuda()

    optimizer = AdamNormGrad(model.parameters(), lr=args.lr)

    # ======================================================================================================================

    print('perform experiment')
    from utils.experiments import experiment_vae
    experiment_vae(args, train_loader, val_loader, test_loader, model, optimizer, dir, model_name = args.model_name)
    # ======================================================================================================================

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#

if __name__ == "__main__":
    run(args, kwargs)

# # # # # # # # # # #
# END EXPERIMENTS # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # #
