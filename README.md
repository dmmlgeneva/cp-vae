## **Conditional Prior VAE (CP-VAE)**


Implementation of CP-VAE, based on our paper: **"Data-Dependent Conditional Priors for Unsupervised Learning of Multimodal Data"**