from __future__ import print_function

import torch
from torch.autograd import Variable

from utils.visualization import plot_images
import numpy as np

import time

import os
from torch.nn import functional as F
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

# ======================================================================================================================
def evaluate_vae(args, model, train_loader, data_loader, epoch, dir, mode):
    # set loss to 0
    evaluate_loss = 0
    evaluate_re = 0
    evaluate_kl = 0
    evaluate_kl_cont = 0
    evaluate_kl_discr = 0
    # set model to evaluation mode
    model.eval()

    # evaluate
    for batch_idx, (data, target) in enumerate(data_loader):
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data, volatile=True), Variable(target)

        x = data

        # calculate loss function
        loss, RE, KL, KL_cont, KL_discr = model.calculate_loss(x, average=True)

        evaluate_loss += loss.data[0]
        evaluate_re += -RE.data[0]
        evaluate_kl += KL.data[0]
        if args.prior == 'MoG' or args.prior == 'standard':
            evaluate_kl_discr = 0.0
            evaluate_kl_cont = 0.0
        else:
            evaluate_kl_cont += KL_cont.data[0]
            evaluate_kl_discr += KL_discr.data[0]

        # print N digits
        if batch_idx == 1 and mode == 'validation':
            if epoch == 1:
                pass
                if not os.path.exists(dir + 'figures/'):
                    os.makedirs(dir + 'figures/')
                # VISUALIZATION: plot real images
                plot_images(args, data.data.cpu().numpy()[0:9], dir + 'figures/', 'real', size_x=3, size_y=3)

            if epoch%100 == 0 :
                x_mean = model.reconstruct_x(x)
                plot_images(args, x_mean.data.cpu().numpy()[0:9], dir + 'figures/', 'reconstruction_validation_' + str(epoch), size_x=3, size_y=3)

    if mode == 'test':
        # load all data
        # grab the test data by iterating over the loader
        # there is no standardized tensor_dataset member across pytorch datasets
        test_data, test_target = [], []
        for data, lbls in data_loader:
            test_data.append(data)
            test_target.append(lbls)

        test_data, test_target = [torch.cat(test_data, 0), torch.cat(test_target, 0).squeeze()]

        # grab the train data by iterating over the loader
        # there is no standardized tensor_dataset member across pytorch datasets
        tr_data, tr_target = [], []
        for data, lbls in train_loader:
            tr_data.append(data)
            tr_target.append(lbls)

        tr_data, tr_target = [torch.cat(tr_data, 0), torch.cat(tr_target, 0).squeeze()]


        if args.cuda:
            test_data, test_target, full_data, tr_data, tr_target = \
                test_data.cuda(), test_target.cuda(), full_data.cuda(), tr_data.cuda(), tr_target.cuda()

        if args.dynamic_binarization:
            tr_data = torch.bernoulli(tr_data)

        # print(model.means(model.idle_input))

        # # VISUALIZATION: plot real images
        plot_images(args, test_data.data.cpu().numpy()[0:25], dir + 'figures/', 'real_test', size_x=5, size_y=5)

        # # VISUALIZATION: plot reconstructions
        samples = model.reconstruct_x(test_data[0:25])
        plot_images(args, samples.data.cpu().numpy(), dir + 'figures/', 'reconstructions_test', size_x=5, size_y=5)

        # # VISUALIZATION: plot generations
        if args.model_name == 'vae_z_xc' or args.model_name == 'vae_z_x':
          
            ############################################################################################################
            # VISUALIZATION: plot generations from a specific category
            ############################################################################################################
            for j in range(args.disc_size):
                samples_rand_specific =[]
                for i in range(25):
                    sample_rand_specific, z_sample_rand_discr = model.generate_specific_x_cat (j)
                    samples_rand_specific.append(sample_rand_specific)
                sample_rand_specific = torch.cat(samples_rand_specific)
                plot_images(args, sample_rand_specific.data.cpu().numpy(), dir + 'figures/' , 'generations_specific_cat' + str(j), size_x=5, size_y=5)


            ############################################################################################################
            # plot generated samples sampling from the marginal categorical posterior
            ############################################################################################################
            z_q_mean, z_q_logvar, z_q_discr = model.encoder(tr_data)

            q_c_x = F.softmax (z_q_discr, dim=-1)
            average_q_c_x = torch.mean( q_c_x, dim=0)
            q_c = average_q_c_x.detach()
            print(average_q_c_x)

            samples_rand1_marginal =[]
            for i in range(25):
                sample_rand_marginal, z_sample_rand_discr = model.generate_cont_x_marginal (average_q_c_x)
                samples_rand1_marginal.append(sample_rand_marginal)
            samples_rand_marginal = torch.cat(samples_rand1_marginal)
            plot_images(args, samples_rand_marginal.data.cpu().numpy(), dir + 'figures/', 'generations_marginal', size_x=5, size_y=5) #size_x=5, size_y=5)
            #
            # #
            ############################################################################################################
            # plot marginal categorical posterior
            ############################################################################################################
            c = list(range(args.disc_size ))
            
            plt.figure()
            plt.bar(c,q_c.cpu().numpy() )
            plt.savefig(dir + 'figures/' + 'q_c_train' )


            ordered, indices = torch.sort(average_q_c_x, descending=True)

            ############################################################################################################
            ###  if you know the true label Plot the marginal categorical posterior condition on each label
            ## plot q(c ! y = i)  based on training data
            ############################################################################################################
            list_c_tr = []
            for j in range(10):
                t = 0
                average_q_c_x0_tr = 0
                for i in range(len(tr_target)):
                    if tr_target[i] == j:
                        t = t +1
                        average_q_c_x0_tr += q_c_x[i]
            #
                average_q_c_x0_tr = (average_q_c_x0_tr/t).detach()
                list_c_tr.append(average_q_c_x0_tr)
                plt.figure()
                plt.bar(c, average_q_c_x0_tr.cpu().numpy() )
                plt.suptitle('q(c | y = '+str(j)+')')
                plt.savefig(dir + 'figures/' + 'q_c_x'+str(j)+'_tr')
                plt.close()
                ordered, indices = torch.sort(average_q_c_x0_tr, descending=True)
            torch.save(list_c_tr, dir + args.model_name + '.marginal_each_label_train')


            ############################################################################################################
            # #   plot marginal categorical posterior but only the categories with prob higher than 1/args.disc_size
            ############################################################################################################
            q_c_new = average_q_c_x
            q_c_new[q_c_new < 1/args.disc_size]=0 # uncomment if you want sampling from the whole agr. posterior
            q_c_new = q_c_new.detach()
            count_q_c_new = 0
            for i in range(args.disc_size):
                if q_c_new[i] > (1/args.disc_size):
                    count_q_c_new += 1

            plt.figure()
            plt.bar(c, q_c_new)
            plt.savefig(dir + 'figures/' + 'q_c_only_hight_prob')
            plt.close()

        elif args.model_name == 'vae':

            if args.prior == 'standard':
                samples_rand = model.generate_x(25)
                plot_images(args, samples_rand.data.cpu().numpy(), dir + 'figures/' , 'generations', size_x=5, size_y=5)

            elif args.prior == 'MoG':
                samples_rand_cz =[]
                for i in range(25):
                    zc_sample_means, _, _, _ = model.generate_x_MoG ()
                    samples_rand_cz.append(zc_sample_means)
                samples_means = torch.cat(samples_rand_cz)
                plot_images(args, samples_means.data.cpu().numpy(), dir + 'figures/', 'generations', size_x=5, size_y=5) #size_x=5, size_y=5)

            # # VISUALIZATION: plot generations from a specific category
                for j in range(args.disc_size):
                    samples_rand_specific =[]
                    for i in range(25):
                        sample_rand_specific = model.generate_specific_category_MoG (j)
                        samples_rand_specific.append(sample_rand_specific)
                    sample_rand_specific = torch.cat(samples_rand_specific)

                    plot_images(args, sample_rand_specific.data.cpu().numpy(), dir + 'figures/' , 'generations_specific_cat' + str(j), size_x=5, size_y=5)
            else:
                raise Exception('Wrong name of the prior!')

       else:
            raise Exception('Wrong name of the model!')

        # CALCULATE lower-bound
        t_ll_s = time.time()
        elbo_test = model.calculate_lower_bound(test_data, MB=args.MB)
        t_ll_e = time.time()
        print('Test lower-bound value {:.2f} in time: {:.2f}s'.format(elbo_test, t_ll_e - t_ll_s))

        t_ll_s = time.time()
        elbo_train = model.calculate_lower_bound(tr_data, MB=args.MB)
        t_ll_e = time.time()
        print('Train lower-bound value {:.2f} in time: {:.2f}s'.format(elbo_train, t_ll_e - t_ll_s))





        
    # calculate final loss
    evaluate_loss /= len(data_loader)  # loss function already averages over batch size
    evaluate_re /= len(data_loader)  # re already averages over batch size
    evaluate_kl /= len(data_loader)  # kl already averages over batch size
    evaluate_kl_cont /= len(data_loader)  # kl of continuous latent already averages over batch size
    evaluate_kl_discr /= len(data_loader)  # kl of discrete latent already averages over batch size

    if mode == 'test':
        return evaluate_loss, evaluate_re, evaluate_kl, evaluate_kl_cont, evaluate_kl_discr, elbo_test, elbo_train
    else:
        return evaluate_loss, evaluate_re, evaluate_kl, evaluate_kl_cont, evaluate_kl_discr
